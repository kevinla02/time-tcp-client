#include "tcp.h"

int main(int argc, char **argv) {
  Tcp client;
  char *host;
  if (argc != 2) {
    std::cout << "Usage: ./client <hostname>";
    exit(1);
  }
  host = argv[1];
  client.Start();
  return 0; 
}
