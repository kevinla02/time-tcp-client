// tcp.h -- interface for creation of tcp web client

#ifndef __tcp_h_
#define __tcp_h_

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include <iostream>

const int kBuffSize = 1500;
const uint16_t kPort = 13;

class Tcp {
  public:
    void Start(char *host);
  private:
    int sockfd;
    char *host_;
    sockaddr_in servaddr;
    void OpenSock();
    void Chatting();
};

#endif