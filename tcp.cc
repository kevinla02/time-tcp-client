#include "tcp.h"

void Tcp::Start(char *host) {
  host_ = host;
  OpenSock();
  Chatting();
  close(sockfd);
}

void Tcp::OpenSock() {
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    std::cout << "Failed at openning socket...\n";
    exit(0);
  }
  // setting host address 
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(kPort);
  if (inet_pton(AF_INET, host_, &servaddr.sin_addr) != 1) {
    std::cout << "Failure at getting address from string...\n";
    exit(0);
  }

  if (connect(sockfd, (sockaddr*)&servaddr, sizeof(servaddr)) < 0) {
    std::cout << "Couldn't connect to server...\n";
    exit(0);
  }
}

void Tcp::Chatting() {
  
}
